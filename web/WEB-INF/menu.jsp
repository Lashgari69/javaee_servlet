<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://MyWebLog.org">MyBlog</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/index.jsp">Home</a></li>
            <li><a href="/register/findAll">Register</a></li>
            <li><a href="/login/login.jsp">Login</a></li>
            <li><a href="/logout">Logout</a></li>
        </ul>
    </div>
</nav>