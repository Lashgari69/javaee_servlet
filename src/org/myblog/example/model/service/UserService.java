package org.myblog.example.model.service;

import org.myblog.example.model.entity.User;
import org.myblog.example.model.repository.UserDA;

import java.util.List;

public class UserService {
    private UserService(){}

    private static final UserService USER_SERVICE = new UserService();

    public static UserService getInstance() {
        return USER_SERVICE;
    }

    public void save(User user) throws Exception {
        try (UserDA userDA = new UserDA()){
            userDA.insert(user);
            userDA.commit();
        }
    }

    public void update(User user) throws Exception {
        try (UserDA userDA = new UserDA()) {
            userDA.update(user);
            userDA.commit();
        }
    }

    public void remove(User user) throws Exception {
        try (UserDA userDA = new UserDA()) {
            userDA.delete(user);
            userDA.commit();
        }
    }

    public List<User> findAll() throws Exception {
        try (UserDA userDA = new UserDA()) {
            return userDA.selectAll();
        }
    }

    public void login(User user) throws Exception {
        try (UserDA userDA = new UserDA()){
            userDA.selectOne(user);
        }
    }
}
