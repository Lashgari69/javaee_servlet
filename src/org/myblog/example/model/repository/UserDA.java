package org.myblog.example.model.repository;

import org.myblog.example.common.InvalidUserNameOrPassword;
import org.myblog.example.common.JDBC;
import org.myblog.example.model.entity.User;

import javax.jws.soap.SOAPBinding;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDA implements AutoCloseable{
    private Connection connection;
    private PreparedStatement preparedStatement;

    public UserDA() throws Exception{
        connection = JDBC.getConnection(JDBC.XE);
    }

    public void insert(User user) throws Exception {
        preparedStatement = connection.prepareStatement("select users_seq.nextval nid from dual");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        user.setId(resultSet.getLong("nid"));
        preparedStatement = connection.prepareStatement("insert into users (id,username,email,password,role_name) values (?,?,?,?,?)");
        preparedStatement.setLong(1, user.getId());
        preparedStatement.setString(2, user.getUsername());
        preparedStatement.setString(3, user.getEmail());
        preparedStatement.setString(4, user.getPassword());
        preparedStatement.setString(5, user.getRoleName());
        preparedStatement.executeUpdate();
    }

    public void update(User user) throws Exception {
        preparedStatement = connection.prepareStatement("update users set username=?, email=?, password=?, role_name=? where id=?");
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, user.getEmail());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRoleName());
        preparedStatement.setLong(5, user.getId());
        preparedStatement.executeUpdate();
    }

    public void delete(User user) throws Exception {
        preparedStatement = connection.prepareStatement("delete from users where id=?");
        preparedStatement.setLong(1, user.getId());
        preparedStatement.executeUpdate();
    }

    public List<User> selectAll() throws Exception {
        preparedStatement = connection.prepareStatement("select * from users");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<User> userList = new ArrayList<>();
        while (resultSet.next()){
            User user = new User().setId(resultSet.getLong("id")).setUsername(resultSet.getString("username"))
                                  .setEmail(resultSet.getString("email")).setPassword(resultSet.getString("password")).setRoleName(resultSet.getString("role_name"));
            userList.add(user);
        }
        return userList;
    }

    public User selectOne(User user) throws Exception {
        preparedStatement = connection.prepareStatement("select * from users where upper(username)=upper(?) and password=?");
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, user.getPassword());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            user.setRoleName(resultSet.getString("role_name"));
            return user;
        }
        throw new InvalidUserNameOrPassword();
    }


    public void commit() throws Exception {
        connection.commit();
    }

    @Override
    public void close() throws Exception {
        preparedStatement.close();
        connection.close();
    }
}
