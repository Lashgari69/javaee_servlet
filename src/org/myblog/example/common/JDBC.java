package org.myblog.example.common;

import org.apache.commons.dbcp2.BasicDataSource;
import org.myblog.example.controller.StartUp;

import java.sql.Connection;

public class JDBC {
    public static final int ORCL = 1;
    public static final int XE = 2;
    private static final BasicDataSource ORCL_DATASOURCE = new BasicDataSource();
    private static final BasicDataSource XE_DATASOURCE = new BasicDataSource();

    static  {
        ORCL_DATASOURCE.setPassword(StartUp.getPassword());
        ORCL_DATASOURCE.setUsername(StartUp.getUsername());
        ORCL_DATASOURCE.setDriverClassName(StartUp.getDriver());
        ORCL_DATASOURCE.setUrl(StartUp.getUrl() + "orcl");
        ORCL_DATASOURCE.setMaxTotal(10);

        XE_DATASOURCE.setPassword(StartUp.getPassword());
        XE_DATASOURCE.setUsername(StartUp.getUsername());
        XE_DATASOURCE.setDriverClassName(StartUp.getDriver());
        XE_DATASOURCE.setUrl(StartUp.getUrl() + "xe");
        XE_DATASOURCE.setMaxTotal(10);
    }

    public static Connection getConnection(int db) throws Exception {
        Connection connection = null;
        switch (db) {
            case 1:
                connection = ORCL_DATASOURCE.getConnection();
                connection.setAutoCommit(false);
                return connection;
            default:
                connection = XE_DATASOURCE.getConnection();
                connection.setAutoCommit(false);
                return connection;
        }
    }
}
