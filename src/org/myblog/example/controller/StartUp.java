package org.myblog.example.controller;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/startup.run", loadOnStartup = 1)
public class StartUp extends HttpServlet {
    private static String username;
    private static String password;
    private static String url;
    private static String driver;

    public static String getUsername() {return username;}

    public static String getPassword() {return password;}

    public static String getUrl() {return url;}

    public static String getDriver() {return driver;}

    @Override
    public void init(ServletConfig config) throws ServletException{
        username = config.getServletContext().getInitParameter("username");
        password = config.getServletContext().getInitParameter("password");
        url = config.getServletContext().getInitParameter("url");
        driver = config.getServletContext().getInitParameter("driver");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        super.service(req, resp);
    }
}
