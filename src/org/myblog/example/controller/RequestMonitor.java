package org.myblog.example.controller;

import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.model.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestMonitor implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException{
        //todo
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (request.getSession().getAttribute("user") == null) {
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        } else {
            String panelName = request.getRequestURI().split("/")[1];
            if (panelName.equals(((User) request.getSession().getAttribute("user")).getRoleName())) {
                filterChain.doFilter(request, response);
            } else {
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            }

        }
    }

    @Override
    public void destroy() {
        //todo
    }
}
