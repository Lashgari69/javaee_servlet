package org.myblog.example.controller;

import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.model.entity.User;
import org.myblog.example.model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register/remove")
public class Remove extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user = new User().setId(Long.parseLong(req.getParameter("id")));
            UserService.getInstance().remove(user);
            resp.sendRedirect("/register/findAll");
        }catch (Exception e) {
            req.setAttribute("msg" , ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}
