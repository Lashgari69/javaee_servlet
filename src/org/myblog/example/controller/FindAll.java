package org.myblog.example.controller;

import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.model.entity.User;
import org.myblog.example.model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/register/findAll")
public class FindAll extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<User> userList = UserService.getInstance().findAll();
            req.setAttribute("list", userList);
            req.getRequestDispatcher("/register/index.jsp").forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req,resp);
        }
    }
}
