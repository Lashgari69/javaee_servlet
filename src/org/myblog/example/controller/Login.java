package org.myblog.example.controller;

import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.model.entity.User;
import org.myblog.example.model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.ImagingOpException;
import java.io.IOException;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        try {
            User user = new User().setUsername(req.getParameter("username")).setPassword(req.getParameter("password"));
            UserService.getInstance().login(user);
            req.getSession().setAttribute("user", user);
            resp.sendRedirect("/"+user.getRoleName()+"/index.jsp");
        }catch (Exception e){
            req.setAttribute("msg" , ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}
