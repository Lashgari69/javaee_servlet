package org.myblog.example.controller;

import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.common.ValidationException;
import org.myblog.example.model.entity.User;
import org.myblog.example.model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register/save")
public class Save extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user;
            if (req.getParameter("username").equals("admin")) {
                user = new User().setUsername(req.getParameter("username")).setEmail(req.getParameter("email")).setPassword(req.getParameter("password")).setRoleName("admin");
            }else {
                user = new User().setUsername(req.getParameter("username")).setEmail(req.getParameter("email")).setPassword(req.getParameter("password")).setRoleName("public");
            }
            if (user.getUsername().isEmpty()) {throw new ValidationException();}

            UserService.getInstance().save(user);
            resp.sendRedirect("/register/findAll");
        }catch (Exception e){
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}
